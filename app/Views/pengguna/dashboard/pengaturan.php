<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Pengaturan
            <small>ourdream.id</small>
        </h1>
        <ol class="breadcrumb">
            <a href="<?= SITE_UNDANGAN ?>/<?= $order[0]->domain ?>" type="button" class="btn btn-primary">Lihat Web</a>
        </ol>
    </section>
    <div><br></div>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pengaturan</h3>
                    </div>
                    <form role="form" action="">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="domain">Url/Link Undangan</label>
                                <br><br>
                                <label style="bottom: -12.3px;position: inherit;padding-left: 5px;color: #005CAA;font-weight: bold;display: table;margin-bottom: -2.7rem;">ourdream.id/</label>
                                <input id="domain" type="text" class="form-control" placeholder="akudandia" style="padding-left: 87px;" value="<?= $order[0]->domain ?>" onkeyup="nospaces(this)" required>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalDomain">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Resepsi Nikah</h3>
                    </div>
                    <form role="form" action="">
                        <div class="box-body">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" disabled checked id="setSampul">
                                    <label class="custom-control-label" for="setSampul">Halaman Sampul</label>
                                </div>
                                <div class="custom-control custom-switch mt-2">
                                    <input type="checkbox" class="custom-control-input" disabled checked id="setMempelai">
                                    <label class="custom-control-label" for="setMempelai">Halaman Mempelai</label>
                                </div>
                                <div class="custom-control custom-switch mt-2">
                                    <input type="checkbox" class="custom-control-input" disabled checked id="setAcara">
                                    <label class="custom-control-label" for="setAcara">Halaman Acara</label>
                                </div>
                                <div class="custom-control custom-switch mt-2">
                                    <input type="checkbox" class="custom-control-input" id="setUcapan" <?php if ($fitur[0]->komen == '1') echo 'checked'; ?>>
                                    <label class="custom-control-label" for="setUcapan">Halaman Ucapan</label>
                                </div>
                                <div class="custom-control custom-switch mt-2">
                                    <input type="checkbox" class="custom-control-input" id="setAlbum" <?php if ($fitur[0]->gallery == '1') echo 'checked'; ?>>
                                    <label class="custom-control-label" for="setAlbum">Halaman Gallery/Album</label>
                                </div>
                                <div class="custom-control custom-switch mt-2">
                                    <input type="checkbox" class="custom-control-input" id="setCerita" <?php if ($fitur[0]->cerita == '1') echo 'checked'; ?>>
                                    <label class="custom-control-label" for="setCerita">Halaman Cerita</label>
                                </div>
                                <div class="custom-control custom-switch mt-2">
                                    <input type="checkbox" class="custom-control-input" id="setLokasi" <?php if ($fitur[0]->lokasi == '1') echo 'checked'; ?>>
                                    <label class="custom-control-label" for="setLokasi">Halaman Lokasi</label>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalFitur">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalDomain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin mengubah nama domain ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-primary" id="simpanDomain">Ya</button>
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalFitur" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menyimpan perubahan ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-primary" id="simpanFitur">Ya</button>
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalGagal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Kesalahan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Gagal mengganti nama domain..
                Nama domain sudah dipakai!!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script>
    function nospaces(t) {
        if (t.value.match(/\s/g)) {
            t.value = t.value.replace(/\s/g, '');
        }
    }

    $('#simpanFitur').on('click', function(event) {

        var ucapan = $('#setUcapan').is(":checked") ? 1 : 0;
        var album = $('#setAlbum').is(":checked") ? 1 : 0;
        var cerita = $('#setCerita').is(":checked") ? 1 : 0;
        var lokasi = $('#setLokasi').is(":checked") ? 1 : 0;

        console.log(ucapan);

        $.ajax({
            url: "<?= base_url('user/update_fitur') ?>",
            method: "POST",
            data: {
                ucapan: ucapan,
                album: album,
                cerita: cerita,
                lokasi: lokasi
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                }
            }
        });

    });

    $('#simpanDomain').on('click', function(event) {

        var domain = $('#domain').val();

        $.ajax({
            url: "<?= base_url('user/update_domain') ?>",
            method: "POST",
            data: {
                domain: domain
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    $('#modalDomain').modal('hide');
                    $('#modalGagal').modal('show');
                }

                console.log($hasil);
            }
        });

    });
</script>
