<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Galeri
            <small>ourdream.id</small>
        </h1>
        <ol class="breadcrumb">
            <a href="<?= SITE_UNDANGAN ?>/<?= $order[0]->domain ?>" type="button" class="btn btn-primary">Lihat Web</a>
        </ol>
    </section>
    <div><br></div>
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Galeri</h3>
                    </div>
                    <form role="form" action="">
                        <div class="box-body">
                            <div class="card-body">
                                <div class="upload-area-bg">
                                    <div class="upload-area do-add-btn">
                                        <div class="upload-area-inner">
                                            <div class="upload-area-icon-main">
                                                <i class="lni-cloud-download"></i>
                                            </div>
                                            <h3 class="upload-area-caption">
                                                <span>Drag and drop files here</span>
                                            </h3>
                                            <p>or</p>
                                            <button type="button" class="upload-area-button btn" style="z-index:9999;">
                                                <span style="color:#fff">Browse files</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div style="text-align: center;">
                                    <img id="loading" src="<?= base_url() ?>/assets/base/img/loading.svg" style="height: 30px;width: 30px; display: none;" />
                                </div>
                                <div id="previewss">
                                    <?php
                                    $kunci = $data[0]->kunci;
                                    for ($a = 1; $a <= 10; $a++) {
                                        $pathName = 'assets/users/' . $kunci . '/album' . $a . '.png';
                                        if (!file_exists($pathName)) continue;
                                    ?>

                                        <div class="preview-uploads" id="preview<?= $a ?>">
                                            <div class="preview-uploads-img">
                                                <span class="preview">
                                                    <img id="img<?= $a ?>" src="<?= base_url() ?>/assets/users/<?= $kunci ?>/album<?= $a ?>.png" style="height: 100%;object-position: center;object-fit: cover;width: 100%;" />
                                                </span>
                                            </div>
                                            <div class="preview-uploads-name">
                                                <b>
                                                    <p class="name" style="line-height: revert;font-size: 12px;">album<?= $a; ?></p>
                                                </b>
                                                <strong class="error text-danger" style="line-height: revert;font-size: 12px;" data-dz-errormessage></strong>
                                                <p class="size" style="line-height: revert;font-size: 12px;">-</p>
                                            </div>
                                            <div class="preview-uploads-delete">
                                                <button id="<?= $a ?>" data-dz-remove class="btn btn-danger delete btnhehe">
                                                    Hapus
                                                </button>
                                            </div>
                                        </div>

                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Vidio Youtube</h3>
                            </div>
                            <form role="form" action="">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Youtube Link</label>
                                        <textarea id="video" class="form-control" rows="5" placeholder="Contoh : https://youtu.be/zlKzyYnhu-s" required><?= $data[0]->video ?></textarea>
                                    </div>
                                    <a href="<?php echo base_url('youtube'); ?>" style="margin-top: 105px;color: #2c3e50;position: relative;top:3px;color:#17a2b8;"><i class="lni-question-circle" style="color:#17a2b8;"></i>&nbsp Cara Menambahkan Video</a>
                                </div>
                                <div class="box-footer">
                                    <a href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalVideo">Simpan</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Musik</h3>
                            </div>
                            <form role="form" method="post" action="<?= base_url('user/update_musik'); ?>" enctype="multipart/form-data">
                                <div class="box-body">
                                    <audio controls>
                                        <source src="<?= base_url() ?>/assets/users/<?= $data[0]->kunci ?>/musik.mp3" type="audio/mpeg">
                                    </audio>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Pilih Musik</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="musik" id="musik" accept="audio/mpeg">

                                                <?php if (!empty(session()->getFlashdata('error'))) : ?>
                                                    <span style="color: red;"><?= session()->getFlashdata('error') ?></span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Update Musik</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin menyimpan perubahan ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn  btn-primary" id="simpanVideo">Ya</button>
                <button type="button" class="btn  btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>/assets/base/js/dropzone.js"></script>
<script>
    var myDropzone = new Dropzone(document.body, {
        url: "<?php echo base_url('user/update_gallery'); ?>",
        paramName: "file",
        acceptedFiles: 'image/*',
        autoQueue: true,
        maxFilesize: 2, //ukuran maksimal foto
        clickable: ".do-add-btn"
    });

    myDropzone.on("success", function(file, response) {
        if (response == "") {
            $('.dz-preview').remove();
            alert('Batas Upload 10 Foto!');

        } else {
            var aql = JSON.parse(response);
            $('.dz-preview').remove();
            $("#previewss").prepend('<div id="preview' + aql.no + '" class="file-row preview-uploads"><div class="preview-uploads-img"><span class="preview"><img id="img3" src="<?= base_url() ?>/assets/users/' + aql.kunci + '/album' + aql.no + '.png"  style="height: 100%;object-position: center;object-fit: cover;width: 100%;" /></span></div><div class="preview-uploads-name"><p class="name" style="line-height: revert;font-size: 12px;" data-dz-name>album' + aql.no + '</p><strong class="error text-danger" style="line-height: revert;font-size: 12px;"  ></strong><p class="size" style="line-height: revert;font-size: 12px;" >-</p></div><div  class="preview-uploads-delete"><button id="' + aql.no + '" class="btn btn-danger delete btnhehe">Hapus</button></div></div>');
        }
        $('#loading').hide();
    });

    myDropzone.on("sending", function(file, xhr, formData) {
        $('.dz-preview').remove();
        formData.append("kunci", "<?= $kunci ?>");
        $('#loading').show();
    });


    myDropzone.on("error", function(file, response) {
        $('.dz-preview').remove();
        alert('Maximal File = 2MB!');
        $('#loading').hide();
    });

    $(document).on('click', '.btnhehe', function() {

        var button_id = $(this).attr("id");
        var kunci = "<?= $kunci ?>";
        $.ajax({
            type: 'POST',
            url: '<?= base_url('user/del_gallery') ?>',
            data: {
                id: button_id,
                kunci: kunci
            },
            success: function(data) {
                console.log('success: ' + data);
                $('#preview' + button_id).remove();
            }
        });

    });

    $('#simpanVideo').on('click', function(event) {
        var video = $('#video').val();
        $.ajax({
            url: "<?= base_url('user/update_video') ?>",
            method: "POST",
            data: {
                video: video
            },
            async: true,
            dataType: 'html',
            success: function($hasil) {
                if ($hasil == 'sukses') {
                    location.reload();
                } else {
                    alert('Gagal Menyimpan Vidio')
                }
            }
        });

    });
</script>
