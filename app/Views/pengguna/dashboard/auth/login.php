<body class="hold-transition login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="login-box-body" style="text-align: center;">
            <img src="<?= base_url('assets/adminlte/dist/img/avatar5.png') ?>" width="200" height="200">

            <p class="login-box-msg">Sign in to start your session</p>

            <?php
            $session = session();
            $errors = $session->getFlashdata('errors');
            if ($errors != null) : ?>
                <div class="alert alert-danger" role="alert" id="ikierror">
                    <span class="mb-0">
                        <strong>Error!<strong>
                                <?php
                                foreach ($errors as $err) {
                                    echo $err;
                                }
                                ?>
                    </span>
                </div>
            <?php endif ?>

            <form action="<?= base_url('do_auth') ?>" method="post">
                <div class="form-group has-feedback">
                    <input type="email" name="email" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <input id="submitSignIn" type="submit" class="btn btn-primary btn-block" value="Sign In">
            </form>
        </div>
    </div>
</body>
