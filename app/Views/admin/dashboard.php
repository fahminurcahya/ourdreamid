<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>ourdream.id</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3><?= rupiah($totalPemasukan) ?></h3>

                                <p>Total Pemasukan</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3><?= count($join) ?></h3>

                                <p>Total Pengguna</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3><?= $totalPending ?></h3>

                                <p>Total Pending</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pengguna</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No Invoice</th>
                                    <th>Pengguna</th>
                                    <th>Domain</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $limit = 0;
                                foreach ($join as $row) {
                                    $limit++;
                                    if ($limit > 5) { //limit 4
                                        break;
                                    } ?>
                                    <tr>
                                        <td>#<?= $row->invoice ?></td>
                                        <td><?= $row->username ?></td>
                                        <td><a href="<?= SITE_UNDANGAN . '/' . $row->domain ?>"><?= $row->domain ?></a></td>

                                        <?php if ($row->statusPembayaran == '1') { ?>
                                            <td><span class="badge alert-warning">Menunggu Konfirmasi</span></td>
                                        <?php } else if ($row->statusPembayaran == '2') { ?>
                                            <td><span class="badge alert-success">Lunas</span></td>
                                        <?php } else { ?>
                                            <td><span class="badge alert-secondary">Belum Lunas</span></td>
                                        <?php } ?>

                                        <td>
                                            <div class="btn-group mb-1">
                                                <button type="button" id="btnLihat" class="btn btn-primary btn-sm" data-nama="<?= $row->nama_lengkap ?>" data-bank="<?= $row->nama_bank ?>" data-invoice="<?= $row->invoice ?>">
                                                    Lihat
                                                </button>
                                                <?php if ($row->statusPembayaran == '1') { ?>
                                                    <button style="margin-left: 10px;" type="button" class="btn btn-success btn-sm" data-id="<?= $row->id_user ?>" data-toggle="modal" data-target="#modalKonfirmasi">
                                                        Konfirmasi
                                                    </button>
                                                <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>


                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <script src="<?= base_url('assets/adminlte/bower_components/jquery/dist/jquery.min.js') ?>"></script>
        <!-- jQuery UI 1.11.4 -->
        <script>
            $('#btnLihat').click(function() {
                $("#modalData").modal();
                var nama = document.getElementById("btnLihat").getAttribute("data-nama");
                var bank = document.getElementById("btnLihat").getAttribute("data-bank");
                var invoice = document.getElementById("btnLihat").getAttribute("data-invoice");
                $('#nama_lengkap').val(nama);
                $('#nama_bank').val(bank);
                $('#bukti').attr('src', '<?= base_url() ?>/assets/bukti/' + invoice + '.png');
            })
        </script>

        <!-- /.row -->
    </section>
</div>

<!-- Modal -->
<div class="modal fade" id="modalKonfirmasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Peringatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah kamu yakin ingin mengkonfirmasi pengguna ?
                <input type="hidden" value="" id="iduser">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-primary" id="konfirmasi">Ya</button>
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input id="nama_lengkap" type="text" class="form-control" placeholder="Contoh : Dinda Rahma" value="" required>
                </div>
                <div class="form-group">
                    <label>Nama Bank</label>
                    <input id="nama_bank" type="text" class="form-control" placeholder="Contoh : BRI Syariah " value="" required>
                </div>
                <div class="form-group mb-2">
                    <label>Bukti Transfer</label><br>
                    <img id="bukti" src="" height="250px">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</div>
