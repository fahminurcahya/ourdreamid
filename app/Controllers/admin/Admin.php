<?php

namespace App\Controllers\admin;

use App\Controllers\BaseController;
use CodeIgniter\Controller;
use App\Models\admin\AdminModel;


class Admin extends BaseController
{
    protected $request;

    public function __construct()
    {
        //mengisi variable global dengan data
        $this->session = session();
        $this->AdminModel = new AdminModel();
        $this->request = \Config\Services::request(); //memanggil class request
        $this->uri = $this->request->uri; //class request digunakan untuk request uri/url
        // $this->session->set('uname_admin','mantab');
        // $this->session->set('id_admin','1');
    }

    public function index()
    {
        echo 'URL Tidak Valid / Kurang Lengkap';
    }

    public function login()
    {
        if (session()->has('odAdmin')) {
            return redirect()->to(base_url('admin/dashboard'));
        }
        $data['title'] = 'Selamat Datang';
        $data['view'] = 'admin/auth/login';
        return view('admin/auth/layout', $data);
    }

    public function dashboard()
    {
        if (!session()->has('odAdmin')) {
            return redirect()->to(base_url('login'));
        }

        $data['view'] = 'admin/index';
        $data['join'] = $this->AdminModel->get_all_join();
        $data['totalPending'] = $this->AdminModel->get_total_pending();
        $data['totalPemasukan'] = $this->AdminModel->get_total_pemasukan();
        $data['title'] = 'Admin Dashboard';
        $data['view'] = 'admin/dashboard';
        return view('admin/layout', $data);
    }

    public function do_auth()
    {

        $data['email'] = $this->request->getPost('email');
        $data['password'] = md5($this->request->getPost('password'));
        $hasil = $this->AdminModel->get_admin($data);

        if (count($hasil) > 0) {
            // set session
            $sess_data = array('odAdmin' => TRUE, 'uname_admin' => $hasil[0]->username, 'id_admin' => $hasil[0]->id);
            $this->session->set($sess_data);
            return redirect()->to(base_url('admin/dashboard'));
            exit();
        } else {
            $this->session->setFlashdata('errors', ['Password Salah']);
            return redirect()->to(base_url('/login'));
        }
    }

    public function do_unauth()
    {

        $this->session->destroy();
        return redirect()->to(base_url('/login'));
    }

    public function pengguna()
    {
        $data['title'] = 'Data Pengguna';
        $data['view'] = 'admin/pengguna/pengguna';
        $data['join'] = $this->AdminModel->get_all_join();
        return view('admin/layout', $data);
    }

    public function editPengguna()
    {
        $data['title'] = 'Data Pengguna';
        $data['view'] = 'admin/pengguna/pengguna';
        $data['join'] = $this->AdminModel->get_all_join();
        return view('admin/layout', $data);
    }
}
