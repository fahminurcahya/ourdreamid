<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $data['title'] = 'Data Pembayaran';
        $data['view'] = 'base/admin/dashboard';
        return view('base/admin/layout', $data);
    }

    public function do_auth()
    {

        $data['email'] = $this->request->getVar('username');
        $data['password'] = md5($this->request->getPost('password'));
        $hasil = $this->AdminModel->get_admin($data);
        dd($hasil);

        if (count($hasil) > 0) {
            // set session
            $sess_data = array('masukAdmin' => TRUE, 'uname_admin' => $hasil[0]->username, 'id_admin' => $hasil[0]->id);
            $this->session->set($sess_data);
            return redirect()->to(base_url('admin/dashboard'));
            exit();
        } else {
            $this->session->setFlashdata('errors', ['Password Salah']);
            return redirect()->to(base_url('/login'));
        }
    }
}
